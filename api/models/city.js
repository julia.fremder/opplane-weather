module.exports = (sequelize, DataTypes) => {
  const city = sequelize.define(
    'city',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        unique: true,
        allowNull: false,
      },
      name: {
        type: DataTypes.TEXT,
        allowNull: false,
      },
      temp_c: {
        type: DataTypes.FLOAT,
        allowNull: false,
      },
      temp_f: {
        type: DataTypes.FLOAT,
        allowNull: false,
      },
      city_params: {
        type: DataTypes.TEXT,
        allowNull: false,
      },
      createdAt: {
        type: DataTypes.DATE
      },
    },
    {
      undescored: true,
      paranoid: true,
      tableName: 'city',
      timestamps: false,
    }
  );
  return city;
};
