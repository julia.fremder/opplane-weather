const axios = require('axios').default;

const url = 'http://api.weatherapi.com/v1/current.json';

//axios conection with params from the route
const getWeather = async (city) => {
  return axios.get(url, city);
};

module.exports = {
  getWeather,
};
