const db = require('../models');
const apiWeatherController = require('../controllers/apiWeather.controller');

const checkReturnedInfo = (returnedInfo) => {
  return returnedInfo.name ? true : false;
};

const cityRegistered = async (city) => {
  //check if the city is already at db
  const cityFromDB = await db.city.findOne({
    where: {
      city_params: city,
    },
  });

  return cityFromDB ? cityFromDB.dataValues : 'newCity';
};

const cityWeatherRegistration = async (city, dateNow) => {
  const cityFromDB = await cityRegistered(city);
  if (cityFromDB !== 'newCity') {
    const isGreaterThan30 = (date1, date2) => {
      return (
        (Math.abs(date1.getTime() - date2.getTime()) / (1000 * 3600 * 24)) *
        1440
      );
    };
    const date2 = cityFromDB.createdAt;
    const isGreater = isGreaterThan30(dateNow, date2) > 30;
    if (isGreater) {
      await db.city.destroy({ where: { city_params: city } });
      const returnedInfo = await apiWeatherController.getWeather(city);
      const { name, temp_c, temp_f } = returnedInfo;
      const Registration = {
        name,
        temp_c,
        temp_f,
        city_params: city,
        createdAt: dateNow,
      };
      return db.city.create(Registration);
    } else {
      return {
        message: 'This city was registered in less than 30 minutes.',
        data: cityFromDB,
      };//close return
    }//close else
  } //close verifyCityDB !== newCity

  if (cityFromDB === 'newCity') {
    const returnedInfo = await apiWeatherController.getWeather(city);
    const check = checkReturnedInfo(returnedInfo);
    if (check) {
      const newCityToDB = {
        name: returnedInfo.name,
        temp_c: returnedInfo.temp_c,
        temp_f: returnedInfo.temp_f,
        city_params: city,
        createdAt: dateNow,
      };
      return db.city.create(newCityToDB);
    } else {
      return {
        message: returnedInfo.data.response.data.error.message,
        data: {
          name: returnedInfo.data.response.data.error.message,
        }, //close data
      }; //close return
    } //close else
  } //close if cityFromDB
}; //close cityWeatherRegistration

module.exports = {
  cityWeatherRegistration,
};
