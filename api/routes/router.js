const {Router} = require('express');
const cityController = require('../controllers/city.controller')


module.exports = (app) => {

    const router = Router();

    router.route('/').get((req, res)=> {
        res.send('Server is on!')
    });

    router.route('/weather/:city')
    .post(
        cityController.cityRegistration
        );
    
    app.use('/', router);
}