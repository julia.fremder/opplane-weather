const cityService = require('../services/city.service');

const cityRegistration = async (req, res, next) => {
  try {
    const { city } = req.params;
    const dateNow = new Date();
    const data = await cityService.cityWeatherRegistration(city, dateNow);
    return res.status(200).send({
      data,
    });
  } catch (error) {
    return res.status(500).send({
      message: 'Something went wrong in cityRegistration function.',
      error,
    });
  }
};

module.exports = {
  cityRegistration,
};
