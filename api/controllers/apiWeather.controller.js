const apiWeatherService = require('../services/apiWeather.service');

const getWeather = async (city) => {
    try {
      //api weather
      const returnedData = await apiWeatherService.getWeather({
        params: { key: `${process.env.MY_KEY}`, q: city },
      });
      return {
        mensagem: 'See the weather',
        name: returnedData.data.location.name,
        temp_c: returnedData.data.current.temp_c,
        temp_f: returnedData.data.current.temp_f,
      };
    } catch (error) {
      return { message: 'Internal server error!!', data: error };
    }
  };

module.exports = {
  getWeather,
};
