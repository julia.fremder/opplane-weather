//imports
const express = require('express');
const cors = require('cors');
const router = require('./routes/router');

//settings
const app = express();
const port = process.env.PORT || 3001;

//middlewares
app.use(express.json());
app.use(cors());

//routes
router(app)

//server
app.listen(port);

module.exports = app;