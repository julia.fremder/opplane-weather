'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('city', {
      
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.TEXT,
        allowNull: false,
      },
      temp_c: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      temp_f: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      city_params: {
        type: Sequelize.TEXT,
        allowNull: false,
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      }
      
    });
     
  
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable('city')
  }
};
