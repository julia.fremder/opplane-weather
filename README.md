# Opplane-weather

Application built to show the weather in a particular city.

##### Weather API
It is necessary to register yourself to wheaterapi.com and get an authorization key.
Define a variable called MY_KEY=<'your key here'> in .env file

##### Install MySql
This program runs sequelize and create a MySql DB to deal with data.
Configure your variables:
DB_NAME=db_your_db_name
DB_USERNAME=your_my_sql_user
DB_PASSWORD=password
DB_HOST=localhost
DB_PORT=3306
DB_DIALECT=mysql

##### Create DB
Runs the comand 'sequelize db:create'

##### Create tables
Run the comand 'sequelize db:migrate'

#### Open postman or insomnia and configure a post route
route: localhost:3001/weather/:city
your city is going to be saved in the database together with weather information